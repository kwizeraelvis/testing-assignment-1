package com.group.assignment.service;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class CalculatorServiceTest {

    private static CalculatorService calculatorService;

    @BeforeAll
    static void beforeAll() {
        calculatorService = new CalculatorServiceImpl();
    }

     @Test
    void testAdd() {
        Integer sum = calculatorService.add(1,2);
        assertEquals(3, sum);
    }

    @Test
    void testSubtract() {
        Integer difference = calculatorService.subtract(3, 2);
        assertEquals(1, difference);
    }

    @Test
    void testDivideWithNoZeroDivision() {
        Float quotient = calculatorService.divide(6, 3);
        assertEquals(2, quotient);
    }

    @Test
     void testDivideWithZeroDivision(){
        ArithmeticException arithmeticException = assertThrows(ArithmeticException.class, () -> calculatorService.divide(2, 0));
        assertEquals("Division by zero is illegal", arithmeticException.getMessage());
    }

    @Test
    void product() {
        Integer product = calculatorService.product(6, 2);
        assertEquals(12, product);
        assertNotNull(product);
    }
}
