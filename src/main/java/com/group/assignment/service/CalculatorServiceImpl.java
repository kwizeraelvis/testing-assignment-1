package com.group.assignment.service;

public class CalculatorServiceImpl implements CalculatorService{

    @Override
    public Integer add(Integer a, Integer b) {
        return a + b;
    }

    @Override
    public Integer subtract(Integer a, Integer b) {
        return a - b;
    }

    @Override
    public Float divide(Integer a, Integer b) {
        if (b == 0){
            throw new ArithmeticException("Division by zero is illegal");
        }
        return (float) (a / b);
    }

    @Override
    public Integer product(Integer a, Integer b) {
        return a * b;
    }
}
