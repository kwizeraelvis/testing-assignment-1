package com.group.assignment.service;

public interface CalculatorService {
    Integer add(Integer a, Integer b);
    Integer subtract(Integer a, Integer b);
    Float divide(Integer a, Integer b);
    Integer product(Integer a, Integer b);
}
